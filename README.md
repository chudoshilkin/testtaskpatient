Web сервис:

1) принимает на вход HL7 (v.2.*) сообщения в виде строки. 

2) парсит их с помощью библиотеки HAPI.

3) сохраняет результат в виде сериализованного объекта, сохраненного в файле.

Web service:

1) accepts HL7 (V. 2.*) message as a string.

2) parses them using the HAPI library.

3) stores the result in the form of a serialized object stored in the file.
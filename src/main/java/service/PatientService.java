package service;

import entity.Patient;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

@LocalBean
@Stateful
public class PatientService {

    private String directoryName = "patients";

    public List<Patient> search() throws IOException, ClassNotFoundException {

        List<Patient> patientList = new ArrayList<>();

        File directory = new File(directoryName);

        if (directory.isDirectory()) {
            String files[] = directory.list();
            for (String file : files) {
                FileInputStream f = new FileInputStream(directoryName + "/" + file);
                ObjectInputStream oin = new ObjectInputStream(f);
                patientList.add((Patient) oin.readObject());
            }
        }

        return patientList;
    }

    public Patient save(Patient patient) throws IOException {

        File directory = new File(directoryName);

        if (!directory.isDirectory()) {
            directory.mkdir();
        }

        FileOutputStream fos = new FileOutputStream(directoryName + "/patient_" + patient.getId());
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(patient);
        oos.close();

        return patient;
    }
}

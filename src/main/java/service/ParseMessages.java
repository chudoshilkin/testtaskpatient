package service;

import ca.uhn.hl7v2.DefaultHapiContext;
import ca.uhn.hl7v2.HL7Exception;
import ca.uhn.hl7v2.HapiContext;
import ca.uhn.hl7v2.model.Message;
import ca.uhn.hl7v2.model.primitive.ID;
import ca.uhn.hl7v2.model.v22.datatype.AD;
import ca.uhn.hl7v2.model.v22.datatype.PN;
import ca.uhn.hl7v2.model.v22.datatype.SI;
import ca.uhn.hl7v2.model.v22.message.ADT_A01;
import ca.uhn.hl7v2.model.v22.segment.PID;
import ca.uhn.hl7v2.parser.EncodingNotSupportedException;
import ca.uhn.hl7v2.parser.Parser;
import entity.Patient;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;

@LocalBean
@Stateful
public class ParseMessages {

    public Patient parse_ADT_A01(String msg) {

        if (msg.contains("\n")) {
            msg = msg.replace("\n", "\r");
        }

        HapiContext context = new DefaultHapiContext();

        Parser p = context.getGenericParser();

        Message hapiMsg = null;
        try {
            hapiMsg = p.parse(msg);
        } catch (EncodingNotSupportedException e) {
            e.printStackTrace();
        } catch (HL7Exception e) {
            e.printStackTrace();
        }

        ADT_A01 adtMsg = (ADT_A01)hapiMsg;

        PID pid = adtMsg.getPID();

        PN patientName = pid.getPatientName();
        AD patientAdress = pid.getPatientAddress(0);
        SI patientSI = pid.getSetIDPatientID();
        ID patientID = pid.getSex();

        String id = patientSI.getValue();
        String givenName = patientName.getGivenName().getValue();
        String familyName = patientName.getFamilyName().getValue();
        String city = patientAdress.getCity().getValue();
        String sex = patientID.getValue();

        Patient patient = new Patient(id, givenName, familyName, sex, city);

        return patient;
    }
}

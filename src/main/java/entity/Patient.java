package entity;

import java.io.Serializable;

public class Patient implements Serializable {
    private String id;
    private String name;
    private String family;
    private String sex;
    private String city;

    public Patient(String id, String name, String family, String sex, String city) {
        this.id = id;
        this.name = name;
        this.family = family;
        this.sex = sex;
        this.city = city;
    }

    public String getFamily() {
        return family;
    }

    public void setFamily(String family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}

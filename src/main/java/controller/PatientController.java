package controller;

import entity.Patient;
import org.apache.log4j.Logger;
import service.ParseMessages;
import service.Message;
import service.PatientService;

import javax.ejb.EJB;
import javax.ws.rs.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Path("/patient")
public class PatientController {

    private static Logger LOG = Logger.getLogger(PatientController.class);

    @EJB
    private ParseMessages parseMessages;

    @EJB
    private PatientService patientService;

    @GET
    @Produces("application/json")
    public List<Patient> getPatient() {

        List<Patient> patientList = new ArrayList<>();

        try {
            patientList = patientService.search();
        } catch (IOException | ClassNotFoundException e) {
            LOG.error("Reading is not successful", e);
        }

        return patientList;
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public Patient postPatient(Message message) {

        Patient patient = parseMessages.parse_ADT_A01(message.getMessage());

        try {
            patientService.save(patient);
        } catch (IOException e) {
            LOG.error("Writing is not successful", e);
        }

        return patient;
    }
}

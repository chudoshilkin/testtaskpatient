$(document).ready(function() {

    $("#button-save").click(function(){
        $.postJSON("api/patient", {message: $("#patient-message").val()}, function(data) {
            showPatient();
        });
    });

    showPatient();
});

function showPatient() {
    $.getJSON("api/patient", {}, function(data) {
        $("#table-patient").children("tbody").html(function() {
            var str = "";
            for (var i = 0; i<data.length; i++) {
                str += "<tr>" +
                "<td>" + data[i].id + "</td>" +
                "<td>" + data[i].name + "</td>" +
                "<td>" + data[i].family + "</td>" +
                "<td>" + data[i].sex + "</td>" +
                "<td>" + data[i].city + "</td>" +
                "</tr>";
            }
            return str;
        });
        return false;
    });
}